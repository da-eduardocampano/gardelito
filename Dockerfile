FROM microsoft/dotnet:2.1-aspnetcore-runtime
COPY bin/Release/netcoreapp2.1/publish app
WORKDIR app
ENV SLACK_APIKEY=$SLACK_APIKEY
CMD ASPNETCORE_URLS=http://*:$PORT dotnet gardelito.dll