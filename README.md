## Nuevo proyecto
```
dotnet new webapi
```

## Agregar paquete
```
dotnet add package Serilog
```

## Correr
```
dotnet run
```

## Publicar
```
dotnet publish -c Release
```

## Build docker image
```
docker build gardelito .
```

## Correr con docker
```
docker run -ti -p 5003:80 -e SLACK_APIKEY=XXX -e PORT=80 gardelito
```

## Deploy to heroku

```
# Inicializar cliente heroku
heroku login
heroku container:login

# Taggear la imagen para heroku
docker tag gardelito registry.heroku.com/gardelito/web

# Subir la imagen de docker al registro de imagenes de heroku
docker push registry.heroku.com/gardelito/web

# Desplegar la aplicacion
heroku container:release web --app gardelito

# Ver los logs de heroku
heroku logs --tail -app gardelito
```
