﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Slackbot;
using Serilog;

namespace gardelito
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console()
                .CreateLogger();

            var apiKey = Environment.GetEnvironmentVariable("SLACK_APIKEY");
            RunBot(apiKey);

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseSerilog(Log.Logger)
                .UseStartup<Startup>();

        public static void RunBot(string apiToken)
        {
            var bot = new Bot(apiToken, "gardelito");

            bot.OnMessage += (sender, message) =>
            {
                if (message.MentionedUsers.Any(user => user == "gardelito"))
                {
                    bot.SendMessage(message.Channel, $"Hola {message.User}, en .NET meetup! Son las {DateTime.UtcNow.AddHours(-3).ToString("HH:mm:ss")}");
                }
            };
        }
    }
}
