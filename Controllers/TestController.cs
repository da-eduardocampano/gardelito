﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace gardelito.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public ActionResult<object> Get()
        {
            return new { Message = $"Hola, bienvenido a .NET meetup! Son las {DateTime.UtcNow.AddHours(-3).ToString("HH:mm:ss")}" };
        }
    }
}
